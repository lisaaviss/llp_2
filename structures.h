#include <stdint.h>
#define STRING_TYPE 0
#define INTEGER_TYPE 1
#define FLOAT_TYPE 2


struct query{
    uint8_t command;
    struct filter* filters;
    struct value_setting* settings;
};

struct filter{
    struct filter* next;
    struct comparator* comp_list;
};

struct keyValuePair{
    char* field;
    uint8_t val_type;
    uint64_t int_value;
    float real_value;
};

struct comparator{
    struct comparator* next;
    uint8_t operation;
    struct keyValuePair fv;
};

struct fullcomparator{
    struct fullcomparator* next;
    struct fullcomparator* connected;
    uint8_t operation;
    struct keyValuePair fv;
};

struct value_setting{
    struct value_setting* next;
    struct keyValuePair fv;
};


